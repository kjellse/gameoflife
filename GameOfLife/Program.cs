﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameOfLifeLibrary;
using System.Threading;
using System.IO;

namespace GameOfLife
{
    class Program
    {
        static string buffer = "";
        static void Main(string[] args)
        {
            RunGOL();            
        }

        static void RunGOL()
        {
            int size = 10;
            //Console.SetWindowSize(size+2, size+2);
            Console.OutputEncoding = Encoding.Unicode;
            List<int> survive = new List<int>() { 2,3 };
            List<int> revive = new List<int>() { 3,6 };

           

            Grid grid = new Grid(size, survive, revive,20);
            Random r = new Random();

            for (int i = 0; i < 10; i++)
            {
                grid.GetTile(r.Next(size), r.Next(size)).Revive();
            }

            //grid.ReviveTile(5, 5);
            //grid.ReviveTile(4, 5);
            //grid.ReviveTile(6, 5);
            
            while (true)
            {
                buffer = "";
                CreateBuffer(grid);
                Console.Clear();
                Console.WriteLine(buffer);
                Console.WriteLine(grid.IterationCount);
                PrintEquilibrium(grid);
                Thread.Sleep(200);
                grid = grid.CalculateNextGeneration();
            }
        }

        private static void PrintEquilibrium(Grid grid)
        {
            if (grid.EquilibriumReachedAt != 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Equilibrium bereikt: {0}", grid.EquilibriumReachedAt);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        static void CreateBuffer(Grid grid)
        {
            for (int x = 0; x < grid.Width; x++)
            {
                for (int y = 0; y < grid.Width; y++)
                {
                    AddTileToBuffer(grid.GetTile(x, y));
                }
                buffer += "\n";
            }
        }

        static void AddTileToBuffer(Tile tile)
        {
            char character = (tile.State == (byte)BinaryTileStates.Alive ?'\u2588' : ' ');
            buffer += character;
        }
    }
}
