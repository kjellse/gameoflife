﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public class Grid
    {
        private int width;
        List<int> survive;
        List<int> revive;

        int iterationCount;
        int equilibriumReachedAt;
        int amountOfLiveStates;

        int anomalyChance;

        Grid previousGrid;
        int historyLength;

        bool isStable;

        private Tile[,] tiles;

        public int Width
        {
            get
            {
                return width;
            }
        }

        public int IterationCount
        {
            get
            {
                return iterationCount;
            }
        }

        public int EquilibriumReachedAt
        {
            get
            {
                return equilibriumReachedAt;
            }
        }

        public bool IsStable
        {
            get
            {
                return isStable;
            }
        }
        //only use this constructor for a new game with history(equilibrium detection)
        public Grid(int width, List<int> survive, List<int> revive, int historyLength = 0, int amountOfLiveStates = 1, int anomalyChance = 0)
        {
            this.width = width;
            this.survive = survive;
            this.revive = revive;
            this.historyLength = historyLength;
            this.amountOfLiveStates = amountOfLiveStates;
            this.anomalyChance = anomalyChance;
            tiles = new Tile[width, width];
            InitializeGrid();
        }

        //this constructor is only used internally for each new iteration
        private Grid(Grid previousGrid)
            : this(previousGrid.width, previousGrid.survive, previousGrid.revive, previousGrid.historyLength, previousGrid.amountOfLiveStates)
        {
            this.previousGrid = previousGrid;
            TrimHistoryToSpecifiedSize();
            this.isStable = previousGrid.isStable;
            this.iterationCount = previousGrid.iterationCount + 1;
            this.equilibriumReachedAt = previousGrid.equilibriumReachedAt;
        }

        private void CheckStability()
        {
            Grid prevGrid = this;
            while (prevGrid.previousGrid != null)
            {
                prevGrid = prevGrid.previousGrid;
                if (this.Equals(prevGrid))
                {
                    isStable = true;
                    SetEquilibriumIfNotSetYet();
                    return;
                }
            }
            isStable = false;
            equilibriumReachedAt = 0;
        }

        private void SetEquilibriumIfNotSetYet()
        {
            if (equilibriumReachedAt == 0 && isStable == true)
            {
                equilibriumReachedAt = iterationCount;
            }
        }

        private void TrimHistoryToSpecifiedSize()
        {
            Grid grid = this;
            for (int i = historyLength; i > 0; i--)
            {
                if (grid.previousGrid == null)
                {
                    return;
                }
                grid = grid.previousGrid;
            }
            grid.previousGrid = null;
        }

        private void InitializeGrid()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    tiles[i, j] = TilesFactory.GetTile(amountOfLiveStates, 0, survive, revive, anomalyChance);
                }
            }
        }

        public Tile GetTile(int x, int y)
        {
            return tiles[x, y];
        }

        public Grid CalculateNextGeneration()
        {
            Grid newGen = new Grid(this);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    newGen.tiles[i, j] = tiles[i, j].CreateNextGeneration(GetNeighbours(i, j));
                }
            }
            newGen.CheckStability();
            return newGen;
        }

        private Tile[] GetNeighbours(int i, int j)
        {
            List<Tile> neighbouringTiles = new List<Tile>();
            if (j - 1 >= 0)
            {
                neighbouringTiles.Add(tiles[i, j - 1]);
                if (i - 1 >= 0)
                {
                    neighbouringTiles.Add(tiles[i - 1, j - 1]);
                }
                if (i + 1 < width)
                {
                    neighbouringTiles.Add(tiles[i + 1, j - 1]);
                }
            }
            if (j + 1 < width)
            {
                neighbouringTiles.Add(tiles[i, j + 1]);
                if (i - 1 >= 0)
                {
                    neighbouringTiles.Add(tiles[i - 1, j + 1]);
                }
                if (i + 1 < width)
                {
                    neighbouringTiles.Add(tiles[i + 1, j + 1]);
                }
            }
            if (i - 1 >= 0)
            {
                neighbouringTiles.Add(tiles[i - 1, j]);
            }
            if (i + 1 < width)
            {
                neighbouringTiles.Add(tiles[i + 1, j]);
            }
            return neighbouringTiles.ToArray();
        }

        public override bool Equals(object obj)
        {
            Grid otherGrid = obj as Grid;
            if (otherGrid == null)
            {
                return false;
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (!otherGrid.tiles[i, j].Equals(tiles[i, j]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
