﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public class TernaryTile : Tile
    {
        private List<int> surviveCondition;
        private List<int> reviveCondition;

        private const byte lifeState2 = 2;

        private static List<byte> possibleStates = new List<byte>() { dead, lifeState1, lifeState2 };

        static TernaryTile()
        {
            amountOfLifeStates = 2;
        }

        public TernaryTile(byte state, List<int> survive, List<int> revive)
        {
            State = state;
            this.reviveCondition = revive;
            this.surviveCondition = survive;
        }

        public override Tile CreateNextGeneration(params Tile[] connectingTiles)
        {
            int[] amounts = new int[3];
            //we include other states for compatibility between different types of tiles in the same grid
            int otherStateAmounts = 0;
            int lifeCount;

            foreach (Tile tile in connectingTiles)
            {
                if (possibleStates.Contains(tile.State))
                {
                    amounts[tile.State]++;
                }
                else
                {
                    //we encountered a state unknown to TernaryTile
                    otherStateAmounts++;
                }
            }
            //we assume unknown states to be alive
            lifeCount = amounts[lifeState1] + amounts[lifeState2] + otherStateAmounts;

            if (reviveCondition.Contains(lifeCount) && State == dead)
            {
                //we revive, the state most found among the neighbours is our new state, if equal lifeState1 is used
                if (amounts[lifeState2] > amounts[lifeState1])
                {
                    return new TernaryTile(lifeState2, surviveCondition, reviveCondition);
                }
                return new TernaryTile(lifeState1, surviveCondition, reviveCondition);
            }

            if (surviveCondition.Contains(lifeCount) && State != dead)
            {
                //we survive, our state stays the same
                return new TernaryTile(State, surviveCondition, reviveCondition);
            }
            return new TernaryTile(dead, surviveCondition, reviveCondition);
        }

        public override bool Equals(object obj)
        {
            TernaryTile otherTile = obj as TernaryTile;
            if (otherTile == null)
            {
                return false;
            }
            return (otherTile.State == State ? true : false);
        }
    }
}
