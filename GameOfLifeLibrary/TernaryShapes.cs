﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public class TernaryShapes
    {

        public static void CreateRandomWithLiveChance(int chancePercentage, Grid golGrid)
        {
            if (golGrid == null)
            {
                return;
            }
            int size = golGrid.Width;
            Random r = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int getal = r.Next(100);
                    if (getal < chancePercentage)
                    {
                        byte state = (byte)(r.Next(2) + 1);
                        golGrid.GetTile(i, j).Revive(state);
                    }
                    else
                    {
                        golGrid.GetTile(i, j).Kill();
                    }
                }
            }
        }
    }
}
