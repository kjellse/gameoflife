﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public enum BinaryTileStates
    {
        Dead = 0,
        Alive = 1
    }

    public class BinaryTile : Tile
    {
        static Random random = new Random();
        private List<int> surviveCondition;
        private List<int> reviveCondition;

        static BinaryTile()
        {
            amountOfLifeStates = 1;
        }

        public BinaryTile(byte state, List<int> survive, List<int> revive, int anomalyChance=0)
        {
            this.state = state;
            this.reviveCondition = revive;
            this.surviveCondition = survive;
            this.anomalyChance = anomalyChance;
        }

        public override Tile CreateNextGeneration(params Tile[] connectingTiles)
        {
            bool anomaly = (random.Next(anomalyChance) == 1); // if anomalychance == 0, no anomalies appear else the chance is 1/nomalychance
            int lifeCount = 0;
            foreach (Tile tile in connectingTiles)
            {
                if (tile.State != dead)
                {
                    lifeCount++;
                }
            }
            if ((reviveCondition.Contains(lifeCount) && State == dead) || (surviveCondition.Contains(lifeCount) && State == lifeState1))
            {
                if(!anomaly)
                    return new BinaryTile(lifeState1, surviveCondition, reviveCondition, anomalyChance);
                return new BinaryTile(dead, surviveCondition, reviveCondition, anomalyChance);
            }
            if (!anomaly)
                return new BinaryTile(dead, surviveCondition, reviveCondition, anomalyChance);
            return new BinaryTile(lifeState1, surviveCondition, reviveCondition, anomalyChance);
        }

        public override bool Equals(object obj)
        {
            BinaryTile otherTile = obj as BinaryTile;
            if (otherTile == null)
            {
                return false;
            }
            return (otherTile.State == State ? true : false);
        }
    }
}
