﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public abstract class Tile
    {
        protected const byte dead = 0;
        protected const byte lifeState1 = 1;

        public abstract Tile CreateNextGeneration(params Tile[] connectingTiles);

        protected static byte amountOfLifeStates;

        protected int anomalyChance;
        protected byte state;
        public virtual byte State
        {
            get
            {
                return state;
            }
            internal set
            {
                if (value < 0)
                {
                    state = 0;
                }
                else if (value > amountOfLifeStates)
                {
                    state = amountOfLifeStates;
                }
                else
                {
                    state = value;
                }
            }
        }

        public virtual void Kill()
        {
            State = dead;
        }

        public virtual void Revive(byte state = lifeState1)
        {
            State = state;
        }

        public virtual void ToggleLiveDead(byte state = lifeState1)
        {
            if (State == dead)
            {
                State = state;
            }
            else
            {
                State = dead;
            }
        }

        public virtual void ToggleAllStates()
        {
            State = (byte)((State + 1) % (amountOfLifeStates + 1));
        }

    }
}
