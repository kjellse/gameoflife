﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public static class BinaryShapes
    {
        public static void CreatePulsar(Grid golGrid)
        {
            ClearAllTiles(golGrid);
            if (golGrid.Width < 5)
                return;
            int middle = golGrid.Width / 2;
            golGrid.GetTile(middle + 2, middle - 2).Revive();
            golGrid.GetTile(middle + 2, middle - 1).Revive();
            golGrid.GetTile(middle + 2, middle).Revive();
            golGrid.GetTile(middle + 2, middle + 1).Revive();
            golGrid.GetTile(middle + 2, middle + 2).Revive();
            golGrid.GetTile(middle - 2, middle - 2).Revive();
            golGrid.GetTile(middle - 2, middle - 1).Revive();
            golGrid.GetTile(middle - 2, middle).Revive();
            golGrid.GetTile(middle - 2, middle + 1).Revive();
            golGrid.GetTile(middle - 2, middle + 2).Revive();
            golGrid.GetTile(middle, middle - 2).Revive();
            golGrid.GetTile(middle, middle + 2).Revive();
        }

        public static void CreateAcorn(Grid golGrid)
        {
            ClearAllTiles(golGrid);
            if (golGrid.Width < 7)
                return;
            int middle = golGrid.Width / 2;
            golGrid.GetTile(middle + 1, middle).Revive();
            golGrid.GetTile(middle + 2, middle).Revive();
            golGrid.GetTile(middle + 3, middle).Revive();
            golGrid.GetTile(middle, middle - 1).Revive();
            golGrid.GetTile(middle - 2, middle).Revive();
            golGrid.GetTile(middle - 3, middle).Revive();
            golGrid.GetTile(middle - 2, middle - 2).Revive();
        }

        public static void CreateDieHard(Grid golGrid)
        {
            ClearAllTiles(golGrid);
            if (golGrid.Width < 9)
                return;
            int middle = golGrid.Width / 2;
            golGrid.GetTile(middle - 3, middle).Revive();
            golGrid.GetTile(middle - 2, middle).Revive();
            golGrid.GetTile(middle - 2, middle + 1).Revive();
            golGrid.GetTile(middle + 2, middle + 1).Revive();
            golGrid.GetTile(middle + 3, middle + 1).Revive();
            golGrid.GetTile(middle + 4, middle + 1).Revive();
            golGrid.GetTile(middle + 3, middle - 1).Revive();
        }
        public static void CreateRPentomino(Grid golGrid)
        {
            ClearAllTiles(golGrid);
            if (golGrid.Width < 3)
                return;
            int middle = golGrid.Width / 2;
            golGrid.GetTile(middle, middle).Revive();
            golGrid.GetTile(middle - 1, middle).Revive();
            golGrid.GetTile(middle, middle - 1).Revive();
            golGrid.GetTile(middle, middle + 1).Revive();
            golGrid.GetTile(middle + 1, middle - 1).Revive();
        }

        public static void CreateHorizontalInfinite(Grid golGrid)
        {
            ClearAllTiles(golGrid);
            if (golGrid.Width < 53)
                return;
            int middle = golGrid.Width / 2;
            SetHorizontalLine(middle - 26, middle, 8, golGrid);
            SetHorizontalLine(middle - 17, middle, 5, golGrid);
            SetHorizontalLine(middle - 9, middle, 3, golGrid);
            SetHorizontalLine(middle, middle, 7, golGrid);
            SetHorizontalLine(middle + 8, middle, 5, golGrid);
        }

        private static void SetHorizontalLine(int x, int y, int length, Grid golGrid)
        {
            for (int i = 0; i < length; i++)
            {
                golGrid.GetTile(x + i, y).Revive();
            }
        }

        public static void CreateRandomWithLiveChance(int chancePercentage, Grid golGrid)
        {
            if (golGrid == null)
            {
                return;
            }
            int size = golGrid.Width;
            Random r = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int getal = r.Next(100);
                    if (getal < chancePercentage)
                    {
                        golGrid.GetTile(i, j).Revive();
                    }
                    else
                    {
                        golGrid.GetTile(i, j).Kill();
                    }
                }
            }
        }

        private static void ClearAllTiles(Grid golGrid)
        {
            for (int i = 0; i < golGrid.Width; i++)
            {
                for (int j = 0; j < golGrid.Width; j++)
                {
                    golGrid.GetTile(i, j).Kill();
                }
            }
        }
    }
}
