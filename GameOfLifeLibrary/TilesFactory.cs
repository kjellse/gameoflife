﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeLibrary
{
    public static class TilesFactory
    {
        public static Tile GetTile(int amountOfAliveStates, byte state, List<int> surviveList, List<int> reviveList, int anomalyChance = 0)
        {
            if (amountOfAliveStates == 1)
            {
                return new BinaryTile(state, surviveList, reviveList, anomalyChance);
            }
            if (amountOfAliveStates == 2)
            {
                return new TernaryTile(state, surviveList, reviveList);
            }
            throw new NotSupportedException(String.Format("Tiles with {0} alive states are not supported", amountOfAliveStates));
        }
    }
}
