﻿using GameOfLifeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLifeWinForms
{
    public partial class GolForm : Form
    {
        Timer timer;
        Grid golGrid;
        List<int> currentRevive;
        List<int> currentSurvive;

        private Color Color1
        {
            get;
            set;
        }
        private Color Color2
        {
            get;
            set;
        }
        private int HistoryCount
        {
            get
            {
                return sliderHistoryCount.Value;
            }
            set
            {
                if (value > sliderHistoryCount.Maximum)
                {
                    sliderHistoryCount.Value = sliderHistoryCount.Maximum;
                }
                else if (value < sliderHistoryCount.Minimum)
                {
                    sliderHistoryCount.Value = sliderHistoryCount.Minimum;
                }
                else
                {
                    sliderHistoryCount.Value = value;
                }
            }
        }

        private int VirusSize
        {
            get
            {
                return sliderVirusSize.Value;
            }
            set
            {
                if (value > sliderVirusSize.Maximum)
                {
                    sliderVirusSize.Value = sliderVirusSize.Maximum;
                }
                else if (value < sliderVirusSize.Minimum)
                {
                    sliderVirusSize.Value = sliderVirusSize.Minimum;
                }
                else
                {
                    sliderVirusSize.Value = value;
                }
            }
        }

        private int Interval
        {
            get
            {
                return sliderInterval.Value;
            }
            set
            {
                if (value > sliderInterval.Maximum)
                {
                    sliderInterval.Value = sliderInterval.Maximum;
                }
                else if (value < sliderInterval.Minimum)
                {
                    sliderInterval.Value = sliderInterval.Minimum;
                }
                else
                {
                    sliderInterval.Value = value;
                }
            }
        }

        private int GridSize
        {
            get
            {
                return GolPictureBox.Size.Height / VirusSize;
            }
        }

        private int RandomPercentage
        {
            get
            {
                return sliderRandomPercentage.Value;
            }
            set
            {
                if (value > sliderRandomPercentage.Maximum)
                {
                    sliderRandomPercentage.Value = sliderRandomPercentage.Maximum;
                }
                else if (value < sliderRandomPercentage.Minimum)
                {
                    sliderRandomPercentage.Value = sliderRandomPercentage.Minimum;
                }
                else
                {
                    sliderRandomPercentage.Value = value;
                }
            }
        }

        //this indicates the amount of alive forms a Tile should have
        private int TileTypeAmountAliveStates
        {
            get
            {
                if (rbBinary.Checked)
                {
                    return 1;
                }
                if (rbTernary.Checked)
                {
                    return 2;
                }
                //default
                return 1;
            }
        }

        public GolForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;

            PresetSurviveRevive();
            FillPresetShapes();
            SetColor1To(Color.Red);
            SetColor2To(Color.Green);

            NewGame();

            //DialogResult diares = colorDialog1.ShowDialog();
            //colorDialog1.Color

            timer = new Timer();
            timer.Tick += new EventHandler(NextGeneration);
        }

        
        private void SetColor1To(Color color)
        {
            Color1 = color;
            btnColor1.BackColor = color;
        }

        private void SetColor2To(Color color)
        {
            Color2 = color;
            btnColor2.BackColor = color;
        }
        private void PresetSurviveRevive()
        {
            lstbRevive.SelectedItems.Add("3");
            lstbSurvive.SelectedItems.Add("2");
            lstbSurvive.SelectedItems.Add("3");
        }

        private void FillPresetShapes()
        {
            //TODO: create enum of possible shapes and put it in library. put those enum values in the dropdown
            cmbPresetShapes.Items.Add("Pulsar");
            cmbPresetShapes.Items.Add("Acorn");
            cmbPresetShapes.Items.Add("DieHard");
            cmbPresetShapes.Items.Add("RPentomino");
            cmbPresetShapes.Items.Add("Horizontal");
            cmbPresetShapes.Items.Add("Random");
            cmbPresetShapes.Items.Add("");
        }

        private void NewGame()
        {
            currentRevive = GetReviveList();
            currentSurvive = GetSurviveList();
            ShowCurrentSettings();
            golGrid = new Grid(GridSize, currentSurvive, currentRevive, HistoryCount, TileTypeAmountAliveStates, GetAnomalyChance());
            SetPresetShapeOnGrid();
            DrawScreen();
        }

        private void ShowCurrentSettings()
        {
            string text = "Survive: " + ListToString(currentSurvive);
            text += "\nRevive: " + ListToString(currentRevive);
            text += "\nTile: " + GetSelectedRadioButtonText(grbTileType);
            text += "\nHistoryCount: " + HistoryCount.ToString();
            lblCurrentReviveSurvive.Text = text;
        }

        private string GetSelectedRadioButtonText(GroupBox container)
        {
            foreach (Control control in container.Controls)
            {
                RadioButton rb = control as RadioButton;
                if (rb != null && rb.Checked)
                {
                    return rb.Text;
                }
            }
            return "";
        }

        private string ListToString(List<int> lijst)
        {
            return string.Join("", lijst.Select(l => l.ToString()));
        }

        private List<int> GetReviveList()
        {
            return ConvertListBoxToList(lstbRevive.SelectedItems);
        }

        private List<int> GetSurviveList()
        {
            return ConvertListBoxToList(lstbSurvive.SelectedItems);
        }

        private int GetAnomalyChance()
        {
            decimal test = nmbAnomaly.Value;
            return int.Parse(test.ToString());
        }

        private List<int> ConvertListBoxToList(ListBox.SelectedObjectCollection listBoxList)
        {
            List<int> list = new List<int>();
            foreach (var item in listBoxList)
            {
                //TODO:TryCatch!
                list.Add(int.Parse(item.ToString()));
            }
            return list;
        }

        private void NextGeneration(object sender, EventArgs e)
        {
            golGrid = golGrid.CalculateNextGeneration();
            DrawScreen();
            if (golGrid.IsStable)
            {
                lblStability.Text = "Stable since iteration " + golGrid.EquilibriumReachedAt;
            }
            else
            {
                lblStability.Text = "Unstable";
            }
            timer.Interval = Interval;
            timer.Start();
        }

        private void DrawScreen()
        {
            if (GolPictureBox.Image == null)
            {
                GolPictureBox.Image = new Bitmap(GolPictureBox.Width, GolPictureBox.Height);
            }
            using (Graphics g = Graphics.FromImage(GolPictureBox.Image))
            {
                Brush brush = new SolidBrush(Color1);
                Brush brush2 = new SolidBrush(Color2);

                g.Clear(Color.White);
                for (int i = 0; i < golGrid.Width; i++)
                {
                    for (int j = 0; j < golGrid.Width; j++)
                    {
                        Tile tile = golGrid.GetTile(i, j);
                        //TODO: choose color and get color selection out of this method
                        Rectangle rect = new Rectangle(i * VirusSize, j * VirusSize, VirusSize, VirusSize);
                        if (tile.State == 1)
                        {
                            g.FillRectangle(brush, rect);
                        }
                        if (tile.State == 2)
                        {
                            g.FillRectangle(brush2, rect);
                        }

                    }
                }
            }
            GolPictureBox.Invalidate();
            SetIterationCount();
        }

        private void SetIterationCount()
        {
            lblIterationCount.Text = golGrid.IterationCount.ToString();
        }

        private void sliderVirusSize_ValueChanged(object sender, EventArgs e)
        {
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "Start")
            {
                timer.Interval = Interval;
                timer.Start();
                btnStart.Text = "Stop";
            }
            else
            {
                timer.Stop();
                btnStart.Text = "Start";
            }
        }

        private void sliderInterval_ValueChanged(object sender, EventArgs e)
        {
        }

        private void cmbPresetShapes_SelectedValueChanged(object sender, EventArgs e)
        {
            //only show the random slider if random is selected in the preset shapes
            bool isRandomSelected = ((string)cmbPresetShapes.SelectedItem).Equals("Random");
            SetRandomSliderVisible(isRandomSelected);

        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void SetPresetShapeOnGrid()
        {
            switch ((string)cmbPresetShapes.SelectedItem)
            {
                case "Pulsar":
                    BinaryShapes.CreatePulsar(golGrid);
                    break;
                case "Acorn":
                    BinaryShapes.CreateAcorn(golGrid);
                    break;
                case "DieHard":
                    BinaryShapes.CreateDieHard(golGrid);
                    break;
                case "RPentomino":
                    BinaryShapes.CreateRPentomino(golGrid);
                    break;
                case "Horizontal":
                    BinaryShapes.CreateHorizontalInfinite(golGrid);
                    break;
                case "Random":
                    if (TileTypeAmountAliveStates == 1)
                    {
                        BinaryShapes.CreateRandomWithLiveChance(RandomPercentage, golGrid);
                    }
                    else if (TileTypeAmountAliveStates == 2)
                    {
                        TernaryShapes.CreateRandomWithLiveChance(RandomPercentage, golGrid);
                    }
                    break;
            }
            DrawScreen();
        }

        private void SetRandomSliderVisible(bool visible)
        {
            sliderRandomPercentage.Visible = visible;
            lblRandomSlider.Visible = visible;
        }

        private void GolPictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            int x_coord = e.X;
            int y_coord = e.Y;
            int x_index = x_coord / VirusSize;
            int y_index = y_coord / VirusSize;
            if (x_index >= golGrid.Width || y_index >= golGrid.Width || x_index < 0 || y_index < 0)
            {
                return;
            }
            if (rbStateToggle.Checked)
                golGrid.GetTile(x_index, y_index).ToggleAllStates();
            if (rbState1.Checked)
                golGrid.GetTile(x_index, y_index).ToggleLiveDead(1);
            if (rbState2.Checked)
                golGrid.GetTile(x_index, y_index).ToggleLiveDead(2);

            DrawScreen();
        }

        private void rbBinary_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBinary.Checked)
            {
                grbState.Visible = false;
                rbState1.Checked = true;
            }

        }

        private void rbTernary_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTernary.Checked)
            {
                grbState.Visible = true;
            }
        }

        private void ColorButton_Click(object sender, EventArgs e)
        {
            colorSelector.ShowDialog();
            if (btnColor1.Equals(sender))
            {
                Color1 = colorSelector.Color;
                btnColor1.BackColor = Color1;
            }
            else if(btnColor2.Equals(sender))
            {
                Color2 = colorSelector.Color;
                btnColor2.BackColor = Color2;
            }
            DrawScreen();
        }

        private void nmbAnomaly_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string anomalyString = nmbAnomaly.Value.ToString();
                if(anomalyString.Contains('.') || anomalyString.Contains(','))
                {
                    anomalyString = anomalyString.Split('.', ',')[0];
                    nmbAnomaly.Value = int.Parse(anomalyString);
                }
                
            }
            catch
            {
                nmbAnomaly.Value=0;
            }
        }
    }
}