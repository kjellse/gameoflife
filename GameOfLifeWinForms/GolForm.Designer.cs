﻿namespace GameOfLifeWinForms
{
    partial class GolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GolPictureBox = new System.Windows.Forms.PictureBox();
            this.sliderVirusSize = new System.Windows.Forms.TrackBar();
            this.lblVirusSize = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.sliderInterval = new System.Windows.Forms.TrackBar();
            this.cmbPresetShapes = new System.Windows.Forms.ComboBox();
            this.lblPresetScenario = new System.Windows.Forms.Label();
            this.lstbSurvive = new System.Windows.Forms.ListBox();
            this.lstbRevive = new System.Windows.Forms.ListBox();
            this.lblSurvive = new System.Windows.Forms.Label();
            this.lblRevive = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.sliderRandomPercentage = new System.Windows.Forms.TrackBar();
            this.lblRandomSlider = new System.Windows.Forms.Label();
            this.lblIterationCount = new System.Windows.Forms.Label();
            this.lblCurrentReviveSurvive = new System.Windows.Forms.Label();
            this.lblStability = new System.Windows.Forms.Label();
            this.rbBinary = new System.Windows.Forms.RadioButton();
            this.rbTernary = new System.Windows.Forms.RadioButton();
            this.grbTileType = new System.Windows.Forms.GroupBox();
            this.grbState = new System.Windows.Forms.GroupBox();
            this.rbStateToggle = new System.Windows.Forms.RadioButton();
            this.rbState2 = new System.Windows.Forms.RadioButton();
            this.rbState1 = new System.Windows.Forms.RadioButton();
            this.sliderHistoryCount = new System.Windows.Forms.TrackBar();
            this.lblHistoryCount = new System.Windows.Forms.Label();
            this.colorSelector = new System.Windows.Forms.ColorDialog();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.nmbAnomaly = new System.Windows.Forms.NumericUpDown();
            this.lblAnomaly = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GolPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderVirusSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderRandomPercentage)).BeginInit();
            this.grbTileType.SuspendLayout();
            this.grbState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderHistoryCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbAnomaly)).BeginInit();
            this.SuspendLayout();
            // 
            // GolPictureBox
            // 
            this.GolPictureBox.Location = new System.Drawing.Point(1, 2);
            this.GolPictureBox.Name = "GolPictureBox";
            this.GolPictureBox.Size = new System.Drawing.Size(1000, 1000);
            this.GolPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.GolPictureBox.TabIndex = 0;
            this.GolPictureBox.TabStop = false;
            this.GolPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GolPictureBox_MouseClick);
            // 
            // sliderVirusSize
            // 
            this.sliderVirusSize.Location = new System.Drawing.Point(1070, 86);
            this.sliderVirusSize.Minimum = 2;
            this.sliderVirusSize.Name = "sliderVirusSize";
            this.sliderVirusSize.Size = new System.Drawing.Size(104, 45);
            this.sliderVirusSize.TabIndex = 1;
            this.sliderVirusSize.Value = 10;
            this.sliderVirusSize.ValueChanged += new System.EventHandler(this.sliderVirusSize_ValueChanged);
            // 
            // lblVirusSize
            // 
            this.lblVirusSize.AutoSize = true;
            this.lblVirusSize.Location = new System.Drawing.Point(1080, 70);
            this.lblVirusSize.Name = "lblVirusSize";
            this.lblVirusSize.Size = new System.Drawing.Size(59, 13);
            this.lblVirusSize.TabIndex = 2;
            this.lblVirusSize.Text = "Virus Size: ";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(1087, 131);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(48, 13);
            this.lblInterval.TabIndex = 3;
            this.lblInterval.Text = "Interval: ";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(1237, 147);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // sliderInterval
            // 
            this.sliderInterval.Location = new System.Drawing.Point(1070, 147);
            this.sliderInterval.Maximum = 1000;
            this.sliderInterval.Minimum = 100;
            this.sliderInterval.Name = "sliderInterval";
            this.sliderInterval.Size = new System.Drawing.Size(104, 45);
            this.sliderInterval.TabIndex = 5;
            this.sliderInterval.TickFrequency = 100;
            this.sliderInterval.Value = 200;
            this.sliderInterval.ValueChanged += new System.EventHandler(this.sliderInterval_ValueChanged);
            // 
            // cmbPresetShapes
            // 
            this.cmbPresetShapes.FormattingEnabled = true;
            this.cmbPresetShapes.Location = new System.Drawing.Point(1090, 366);
            this.cmbPresetShapes.Name = "cmbPresetShapes";
            this.cmbPresetShapes.Size = new System.Drawing.Size(121, 21);
            this.cmbPresetShapes.TabIndex = 6;
            this.cmbPresetShapes.SelectedValueChanged += new System.EventHandler(this.cmbPresetShapes_SelectedValueChanged);
            // 
            // lblPresetScenario
            // 
            this.lblPresetScenario.AutoSize = true;
            this.lblPresetScenario.Location = new System.Drawing.Point(1087, 350);
            this.lblPresetScenario.Name = "lblPresetScenario";
            this.lblPresetScenario.Size = new System.Drawing.Size(80, 13);
            this.lblPresetScenario.TabIndex = 7;
            this.lblPresetScenario.Text = "Preset scenario";
            // 
            // lstbSurvive
            // 
            this.lstbSurvive.FormattingEnabled = true;
            this.lstbSurvive.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.lstbSurvive.Location = new System.Drawing.Point(1090, 210);
            this.lstbSurvive.Name = "lstbSurvive";
            this.lstbSurvive.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstbSurvive.Size = new System.Drawing.Size(40, 121);
            this.lstbSurvive.TabIndex = 8;
            // 
            // lstbRevive
            // 
            this.lstbRevive.FormattingEnabled = true;
            this.lstbRevive.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.lstbRevive.Location = new System.Drawing.Point(1169, 210);
            this.lstbRevive.Name = "lstbRevive";
            this.lstbRevive.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstbRevive.Size = new System.Drawing.Size(38, 121);
            this.lstbRevive.TabIndex = 9;
            // 
            // lblSurvive
            // 
            this.lblSurvive.AutoSize = true;
            this.lblSurvive.Location = new System.Drawing.Point(1087, 194);
            this.lblSurvive.Name = "lblSurvive";
            this.lblSurvive.Size = new System.Drawing.Size(43, 13);
            this.lblSurvive.TabIndex = 10;
            this.lblSurvive.Text = "Survive";
            // 
            // lblRevive
            // 
            this.lblRevive.AutoSize = true;
            this.lblRevive.Location = new System.Drawing.Point(1166, 194);
            this.lblRevive.Name = "lblRevive";
            this.lblRevive.Size = new System.Drawing.Size(41, 13);
            this.lblRevive.TabIndex = 11;
            this.lblRevive.Text = "Revive";
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(1241, 364);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(75, 23);
            this.btnRestart.TabIndex = 12;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // sliderRandomPercentage
            // 
            this.sliderRandomPercentage.Location = new System.Drawing.Point(1090, 422);
            this.sliderRandomPercentage.Maximum = 100;
            this.sliderRandomPercentage.Name = "sliderRandomPercentage";
            this.sliderRandomPercentage.Size = new System.Drawing.Size(104, 45);
            this.sliderRandomPercentage.TabIndex = 13;
            this.sliderRandomPercentage.TickFrequency = 10;
            this.sliderRandomPercentage.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sliderRandomPercentage.Value = 50;
            this.sliderRandomPercentage.Visible = false;
            // 
            // lblRandomSlider
            // 
            this.lblRandomSlider.AutoSize = true;
            this.lblRandomSlider.Location = new System.Drawing.Point(1096, 406);
            this.lblRandomSlider.Name = "lblRandomSlider";
            this.lblRandomSlider.Size = new System.Drawing.Size(58, 13);
            this.lblRandomSlider.TabIndex = 14;
            this.lblRandomSlider.Text = "Random %";
            this.lblRandomSlider.Visible = false;
            // 
            // lblIterationCount
            // 
            this.lblIterationCount.AutoSize = true;
            this.lblIterationCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIterationCount.Location = new System.Drawing.Point(1338, 45);
            this.lblIterationCount.Name = "lblIterationCount";
            this.lblIterationCount.Size = new System.Drawing.Size(29, 31);
            this.lblIterationCount.TabIndex = 15;
            this.lblIterationCount.Text = "0";
            // 
            // lblCurrentReviveSurvive
            // 
            this.lblCurrentReviveSurvive.AutoSize = true;
            this.lblCurrentReviveSurvive.Location = new System.Drawing.Point(1228, 252);
            this.lblCurrentReviveSurvive.Name = "lblCurrentReviveSurvive";
            this.lblCurrentReviveSurvive.Size = new System.Drawing.Size(80, 13);
            this.lblCurrentReviveSurvive.TabIndex = 16;
            this.lblCurrentReviveSurvive.Text = "Survive Revive";
            // 
            // lblStability
            // 
            this.lblStability.AutoSize = true;
            this.lblStability.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStability.Location = new System.Drawing.Point(1341, 109);
            this.lblStability.Name = "lblStability";
            this.lblStability.Size = new System.Drawing.Size(122, 31);
            this.lblStability.TabIndex = 17;
            this.lblStability.Text = "Unstable";
            // 
            // rbBinary
            // 
            this.rbBinary.AutoSize = true;
            this.rbBinary.Checked = true;
            this.rbBinary.Location = new System.Drawing.Point(9, 19);
            this.rbBinary.Name = "rbBinary";
            this.rbBinary.Size = new System.Drawing.Size(54, 17);
            this.rbBinary.TabIndex = 18;
            this.rbBinary.TabStop = true;
            this.rbBinary.Tag = "1";
            this.rbBinary.Text = "Binary";
            this.rbBinary.UseVisualStyleBackColor = true;
            this.rbBinary.CheckedChanged += new System.EventHandler(this.rbBinary_CheckedChanged);
            // 
            // rbTernary
            // 
            this.rbTernary.AutoSize = true;
            this.rbTernary.Location = new System.Drawing.Point(9, 42);
            this.rbTernary.Name = "rbTernary";
            this.rbTernary.Size = new System.Drawing.Size(61, 17);
            this.rbTernary.TabIndex = 19;
            this.rbTernary.Tag = "2";
            this.rbTernary.Text = "Ternary";
            this.rbTernary.UseVisualStyleBackColor = true;
            this.rbTernary.CheckedChanged += new System.EventHandler(this.rbTernary_CheckedChanged);
            // 
            // grbTileType
            // 
            this.grbTileType.Controls.Add(this.rbBinary);
            this.grbTileType.Controls.Add(this.rbTernary);
            this.grbTileType.Location = new System.Drawing.Point(1400, 313);
            this.grbTileType.Name = "grbTileType";
            this.grbTileType.Size = new System.Drawing.Size(79, 74);
            this.grbTileType.TabIndex = 20;
            this.grbTileType.TabStop = false;
            this.grbTileType.Text = "Tile type";
            // 
            // grbState
            // 
            this.grbState.Controls.Add(this.rbStateToggle);
            this.grbState.Controls.Add(this.rbState2);
            this.grbState.Controls.Add(this.rbState1);
            this.grbState.Location = new System.Drawing.Point(1380, 422);
            this.grbState.Name = "grbState";
            this.grbState.Size = new System.Drawing.Size(68, 112);
            this.grbState.TabIndex = 21;
            this.grbState.TabStop = false;
            this.grbState.Text = "State";
            this.grbState.Visible = false;
            // 
            // rbStateToggle
            // 
            this.rbStateToggle.AutoSize = true;
            this.rbStateToggle.Location = new System.Drawing.Point(6, 65);
            this.rbStateToggle.Name = "rbStateToggle";
            this.rbStateToggle.Size = new System.Drawing.Size(58, 17);
            this.rbStateToggle.TabIndex = 2;
            this.rbStateToggle.TabStop = true;
            this.rbStateToggle.Text = "Toggle";
            this.rbStateToggle.UseVisualStyleBackColor = true;
            // 
            // rbState2
            // 
            this.rbState2.AutoSize = true;
            this.rbState2.Location = new System.Drawing.Point(6, 42);
            this.rbState2.Name = "rbState2";
            this.rbState2.Size = new System.Drawing.Size(31, 17);
            this.rbState2.TabIndex = 1;
            this.rbState2.TabStop = true;
            this.rbState2.Text = "2";
            this.rbState2.UseVisualStyleBackColor = true;
            // 
            // rbState1
            // 
            this.rbState1.AutoSize = true;
            this.rbState1.Checked = true;
            this.rbState1.Location = new System.Drawing.Point(6, 19);
            this.rbState1.Name = "rbState1";
            this.rbState1.Size = new System.Drawing.Size(31, 17);
            this.rbState1.TabIndex = 0;
            this.rbState1.TabStop = true;
            this.rbState1.Text = "1";
            this.rbState1.UseVisualStyleBackColor = true;
            // 
            // sliderHistoryCount
            // 
            this.sliderHistoryCount.Location = new System.Drawing.Point(1070, 22);
            this.sliderHistoryCount.Maximum = 500;
            this.sliderHistoryCount.Name = "sliderHistoryCount";
            this.sliderHistoryCount.Size = new System.Drawing.Size(104, 45);
            this.sliderHistoryCount.TabIndex = 22;
            this.sliderHistoryCount.TickFrequency = 25;
            this.sliderHistoryCount.Value = 100;
            // 
            // lblHistoryCount
            // 
            this.lblHistoryCount.AutoSize = true;
            this.lblHistoryCount.Location = new System.Drawing.Point(1078, 2);
            this.lblHistoryCount.Name = "lblHistoryCount";
            this.lblHistoryCount.Size = new System.Drawing.Size(73, 13);
            this.lblHistoryCount.TabIndex = 23;
            this.lblHistoryCount.Text = "History Count:";
            // 
            // btnColor1
            // 
            this.btnColor1.Location = new System.Drawing.Point(1550, 492);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(30, 23);
            this.btnColor1.TabIndex = 24;
            this.btnColor1.UseVisualStyleBackColor = true;
            this.btnColor1.Click += new System.EventHandler(this.ColorButton_Click);
            // 
            // btnColor2
            // 
            this.btnColor2.Location = new System.Drawing.Point(1550, 521);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(30, 23);
            this.btnColor2.TabIndex = 25;
            this.btnColor2.UseVisualStyleBackColor = true;
            this.btnColor2.Click += new System.EventHandler(this.ColorButton_Click);
            // 
            // nmbAnomaly
            // 
            this.nmbAnomaly.Location = new System.Drawing.Point(1090, 487);
            this.nmbAnomaly.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nmbAnomaly.Name = "nmbAnomaly";
            this.nmbAnomaly.Size = new System.Drawing.Size(120, 20);
            this.nmbAnomaly.TabIndex = 27;
            this.nmbAnomaly.ValueChanged += new System.EventHandler(this.nmbAnomaly_ValueChanged);
            // 
            // lblAnomaly
            // 
            this.lblAnomaly.AutoSize = true;
            this.lblAnomaly.Location = new System.Drawing.Point(1090, 468);
            this.lblAnomaly.Name = "lblAnomaly";
            this.lblAnomaly.Size = new System.Drawing.Size(47, 13);
            this.lblAnomaly.TabIndex = 28;
            this.lblAnomaly.Text = "Anomaly";
            // 
            // GolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1688, 1018);
            this.Controls.Add(this.lblAnomaly);
            this.Controls.Add(this.nmbAnomaly);
            this.Controls.Add(this.btnColor2);
            this.Controls.Add(this.btnColor1);
            this.Controls.Add(this.lblHistoryCount);
            this.Controls.Add(this.sliderHistoryCount);
            this.Controls.Add(this.grbState);
            this.Controls.Add(this.grbTileType);
            this.Controls.Add(this.lblStability);
            this.Controls.Add(this.lblCurrentReviveSurvive);
            this.Controls.Add(this.lblIterationCount);
            this.Controls.Add(this.lblRandomSlider);
            this.Controls.Add(this.sliderRandomPercentage);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.lblRevive);
            this.Controls.Add(this.lblSurvive);
            this.Controls.Add(this.lstbRevive);
            this.Controls.Add(this.lstbSurvive);
            this.Controls.Add(this.lblPresetScenario);
            this.Controls.Add(this.cmbPresetShapes);
            this.Controls.Add(this.sliderInterval);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.lblVirusSize);
            this.Controls.Add(this.sliderVirusSize);
            this.Controls.Add(this.GolPictureBox);
            this.Name = "GolForm";
            this.Text = "Game of Life";
            ((System.ComponentModel.ISupportInitialize)(this.GolPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderVirusSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderRandomPercentage)).EndInit();
            this.grbTileType.ResumeLayout(false);
            this.grbTileType.PerformLayout();
            this.grbState.ResumeLayout(false);
            this.grbState.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderHistoryCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbAnomaly)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox GolPictureBox;
        private System.Windows.Forms.TrackBar sliderVirusSize;
        private System.Windows.Forms.Label lblVirusSize;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TrackBar sliderInterval;
        private System.Windows.Forms.ComboBox cmbPresetShapes;
        private System.Windows.Forms.Label lblPresetScenario;
        private System.Windows.Forms.ListBox lstbSurvive;
        private System.Windows.Forms.ListBox lstbRevive;
        private System.Windows.Forms.Label lblSurvive;
        private System.Windows.Forms.Label lblRevive;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.TrackBar sliderRandomPercentage;
        private System.Windows.Forms.Label lblRandomSlider;
        private System.Windows.Forms.Label lblIterationCount;
        private System.Windows.Forms.Label lblCurrentReviveSurvive;
        private System.Windows.Forms.Label lblStability;
        private System.Windows.Forms.RadioButton rbBinary;
        private System.Windows.Forms.RadioButton rbTernary;
        private System.Windows.Forms.GroupBox grbTileType;
        private System.Windows.Forms.GroupBox grbState;
        private System.Windows.Forms.RadioButton rbState2;
        private System.Windows.Forms.RadioButton rbState1;
        private System.Windows.Forms.RadioButton rbStateToggle;
        private System.Windows.Forms.TrackBar sliderHistoryCount;
        private System.Windows.Forms.Label lblHistoryCount;
        private System.Windows.Forms.ColorDialog colorSelector;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.NumericUpDown nmbAnomaly;
        private System.Windows.Forms.Label lblAnomaly;

    }
}

